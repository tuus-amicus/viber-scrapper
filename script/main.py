﻿# -*- encoding: utf-8 -*-
import pyautogui
import time
import subprocess
import psutil
from fromImageToText import getTextFromImg
import re
import psutil
import csv
import threading
import os

from settings import *

numbersFile = 'resources/numbers.txt'
dialupImg = 'resources/imgs/dialupNumber.png'
noActiveMoreBtnImg = 'resources/imgs/moreBtnNoActive.png'
activeMoreBtnImg = 'resources/imgs/moreBtnActive.png'
startChatImg = 'resources/imgs/startChat.png'
okBtnImg = 'resources/imgs/okBtn.png'
removeBtnImg = 'resources/imgs/removeBtn.png'


def restartViber():
    print("Перезапускаю Viber")
    pid = getViberPid()
    if pid is not None:
        os.system("taskkill /f /im {}".format(psutil.Process(pid).name()))

    openViber()

def openViber():
    count = 0
    subprocess.Popen(locationOfViber)
    while not findMoreBtn():
        print('Процесс не запущен. Ожидаю...')
        time.sleep(1)
        count += 1
        if count == 10:
            restartViber()
            return
    pyautogui.PAUSE=1
    pyautogui.hotkey('win', 'up')

def getCenterOf(img):
    location = pyautogui.locateOnScreen(img, grayscale=True)
    if location is not None:
        return pyautogui.center(location)
    else:
        return None

def findMoreBtn():
    moreCenter = getCenterOf(noActiveMoreBtnImg)
    if moreCenter is None:
        moreCenter = getCenterOf(activeMoreBtnImg)
    return moreCenter

def clickOnMore():
    if not tryFindViber(2):
        raise Exception('None')
    location = findMoreBtn()
    if location is None:
        raise Exception('None')
    pyautogui.click(location)

def clickOnDialupNumber():
    location = getCenterOf(dialupImg)
    if location is None:
        raise Exception('None')
    pyautogui.click(location)

def getNumbersFrom(file):
    f = open(file)
    lines=[]
    for line in f:
        lines.append(line)
    return lines

def getViberPid():
    print("Пытаюсь найти запущенный процесс")
    pattern = re.compile("[vV]ibe.*?")
    for pid in psutil.pids():
        try:
            p = psutil.Process(pid)
            if pattern.match(p.name()):
                print('Процесс найден.')
                return pid
        except(psutil.NoSuchProcess, psutil.AccessDenied) as err:
            print('Процесс более недоступен или нет прав доступа к нему')
            continue
    print("Процесс не найден.")
    return None

def isViberRunning():
    return True if getViberPid() is not None else False

def isFocusedOnViber():
    return True if findMoreBtn() is not None else False

def findContactByNumber(number):
    pyautogui.typewrite(number.strip())
    if getCenterOf(startChatImg) is None:
        raise Exception('None')
    pyautogui.click(getCenterOf(startChatImg))
    time.sleep(0.2)

def parseAndSaveInfo(infolist, tel_number):
    name = infolist.splitlines()[0]
    time = None

    timePattern = r'[вВ]?.?сет.?:?'
    for line in infolist.splitlines():
        if re.search(r'[вВ]?.?сет.?:?', line) is not None:
            time = re.split(timePattern, line)[-1].strip()
            break
    if time is None:
        time = 'время не определено'

    with open('users.csv', 'a', newline='', encoding="utf-8") as csvfile:
        fieldnames = ['tel', 'nickname','timestamp', 'controlinfo']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=';')
        writer.writerow({'tel': 'tel:{}'.format(tel_number.strip()),
                         'nickname': name,
                         'timestamp': time,
                         'controlinfo': 100})


def takeScreenshot(with_name):
    rightSideLocation = pyautogui.size()

    leftSideLocation = None
    while leftSideLocation is None:
        leftSideLocation = pyautogui.locateOnScreen(activeMoreBtnImg)
        if leftSideLocation is None:
            leftSideLocation = pyautogui.locateOnScreen(noActiveMoreBtnImg)


    x = leftSideLocation[0] + leftSideLocation[2]
    y = leftSideLocation[1] - 5
    witdh = rightSideLocation[1]
    height = leftSideLocation[2]

    im = pyautogui.screenshot(region=(x, y, witdh, height))

    half_the_width = (im.size[0] / 2) - 40
    im = im.crop((83, 0, half_the_width, im.size[1]))

    im.save(with_name)

def getInfoFromScreenshot(img_path, number):
    return getTextFromImg(img_path, number)

def worker(imgpath, number):
    parseAndSaveInfo(getInfoFromScreenshot(imgpath, number), tel_number=number)

def tryFindViber(times):
    count = 0
    while not isFocusedOnViber():
        if count == times:
            return False
        print("Не могу обнаружить viber в поле зрения, проверьте открыто ли оно или может скрыто в трее?")
        time.sleep(1)
        count += 1
    return True

def execute(action, args):
    if not tryFindViber(2):
        raise Exception('None')

    # Remove all numbers from dialup field by pressing remove btn for 2 seconds
    if pyautogui.locateOnScreen(removeBtnImg) is not None:
        pyautogui.mouseDown(getCenterOf(removeBtnImg))
        time.sleep(2.2)
        pyautogui.mouseUp()

    if not tryFindViber(2):
        raise Exception('None')

    action() if args is None else action(args)


if __name__ == "__main__":
    if locationOfViber is None:
       print("Пожалуйста укажите полный путь файла Viber.exe в файле settings для переменной locationOfViber")
       exit()

    print("Если необходимо завершить скрипт, нажмите CTRL+C")
    print("Формирую список номеров, которые буду искать...")

    numbers = list(getNumbersFrom(numbersFile))
    while len(numbers):
        while not tryFindViber(5):
            restartViber()

        if pyautogui.locateOnScreen(okBtnImg) is not None:
            print("Потеряно соединение...")
            pyautogui.click(getCenterOf(okBtnImg))
            number = numbers.pop()
            numbers.append(number)
            time.sleep(0.5)
            continue

        actions =(clickOnMore, clickOnDialupNumber)
        number = numbers.pop()
        try:
            for action in actions:
                execute(action, None)
        
            execute(findContactByNumber, number)
        
            time.sleep(timeout) # Make dynamic. Depends on time updating 'Was online'
            screenshotpath = 'tmp/screenshot{}.png'.format(number.replace('+','').strip())
            execute(takeScreenshot, screenshotpath)
        except:
            numbers.append(number)
            continue
        print("Threading")
        savingThread = threading.Thread(target = worker, args=(screenshotpath, number,))
        savingThread.start()
