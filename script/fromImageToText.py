from PIL import Image, ImageEnhance, Image
from pytesseract import image_to_string
import cv2
import numpy as np
import os


def _process_and_save_img(img_path, to_path):
    """crop, resize, replace color, improve contrast

    crop: remove leading noise part
    resize: tesseract is not good with small fonts
    replace color: replace yellow with white
    improve contrast: make colors more distinct
    """
    img = Image.open(img_path)
    img = img.crop((3, 0, img.size[0], img.size[1]))
    resized = img.resize(
        (img.size[0] * 16, img.size[1] * 16), Image.ANTIALIAS
    )
    r1, g1, b1 = (200, 200, 100)
    r2, g2, b2 = (255, 255, 255)
    data = np.array(resized.convert('RGBA'))
    red, green, blue, alpha = data.T
    yellow_areas = (red > r1) & (green > g1) & (blue > b1)
    data[..., :-1][yellow_areas.T] = (r2, g2, b2)
    contrast = ImageEnhance.Contrast(Image.fromarray(data))
    adjusted = contrast.enhance(2)
    cv_img = np.array(adjusted)
    cv_img = cv_img[:, :, ::-1].copy()
    gray = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(to_path, gray)
    
def getTextFromImg(img_path, number):
    tmpImgPath = 'tmp/processedImg{}.png'.format(number.replace('+','').strip())
    _process_and_save_img(img_path, to_path = tmpImgPath)
    result = image_to_string(Image.open(tmpImgPath), lang='eng+rus', config='-psm 6')
    os.remove(img_path)
    os.remove(tmpImgPath)
    return result
   
