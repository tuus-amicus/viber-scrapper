# Viber-scrapper

## What project is

Script collecting personal information about viber's users. Works with viber app using python-gui lib.
At the input `.txt` file with numbers, for analyzing such info as: name, online status then storing ones 
in `.csv` file with following format:

`tel.number:2222;Name:john;was 2 days ago; 100`

## Preperations

* For correct use of this script, you shall install **tesseract** library & 
add path to it in $PATH env, e.g. `SET "PATH=%path%;%cd%\libs\tesseract-win\"`

* Setup in settings.py location of **viber.exe** and timeout for waiting status updation in viber app (depends on OS and Internet connection)